import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class ExampleOne extends StatefulWidget {
  const ExampleOne({super.key});

  @override
  State<ExampleOne> createState() => _ExampleOneState();
}

class _ExampleOneState extends State<ExampleOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Examples'),
      ),
      body: Column(
        children: [
          Image.asset(
            "assets/images/img1.png",
            width: 150,
            height: 150,
            color: Colors.orange.shade100,
            colorBlendMode: BlendMode.colorBurn,
            errorBuilder: (context, error, stackTrace) {
              return Text("Unable to load image");
            },
          ),
          Image.file(
            File("assets/images/img1.png"),
            width: 150,
            height: 150,
            errorBuilder: (context, error, stackTrace) {
              return Text("Unable to load image");
            },
          ),
          Image.network(
            "https://picsum.photos/seed/picsum/200/300",
            width: 150,
            height: 150,
          ),
          Icon(
            Icons.attach_email_outlined,
            color: Colors.orange.shade200,
            size: 50.0,
          ),
          Container(
            width: 250,
            height: 250,
            // color: Colors.blue.shade100,
            color: Colors.blue[100],
            // color: Colors.black,
            //color: Color.fromARGB(255, 50, 50, 150),
            // color: Color.fromRGBO(255, 0, 0, 0.4),
            // color: Color(0xFFFFFF05),
            child: Align(
              alignment: Alignment(0.5, -0.5),
              child: Text("Text"),
            ),
          )
        ],
      ),
    );
  }
}
