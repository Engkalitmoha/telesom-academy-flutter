import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:google_fonts/google_fonts.dart';

class ExampleTwo extends StatefulWidget {
  const ExampleTwo({super.key});

  @override
  State<ExampleTwo> createState() => _ExampleTwoState();
}

class _ExampleTwoState extends State<ExampleTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Example Two"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "System Font",
            style: TextStyle(
              fontSize: 20,
              fontFamily: 'Roboto',
            ),
          ),
          Text(
            "Custom Font",
            style: TextStyle(
              fontSize: 20,
              fontFamily: 'Somaliland',
            ),
          ),
          Text(
            "Google Font Package",
            style: GoogleFonts.acme(fontSize: 20),
          ),
        ],
      ),
    );
  }
}
