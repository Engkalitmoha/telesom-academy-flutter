import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class ExampleMap extends StatefulWidget {
  const ExampleMap({super.key});

  @override
  State<ExampleMap> createState() => _ExampleMapState();
}

class _ExampleMapState extends State<ExampleMap> {
  final List<String> _items = ["Computer", "Mobile", "Book"];

  final Map<String, int> myproducts = {
    "Iphone 12": 500,
    "Iphone 13": 600,
    "Iphone 14": 700
  };

  Widget createContainer(String title, Color color) {
    return Container(
      margin: EdgeInsets.all(5),
      width: 100,
      height: 50,
      color: color,
      child: Center(child: Text(title)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Example Map"),
      ),
      body: Column(
        children: [
          ..._items.map((item) {
            Color x = Colors.black26;
            return createContainer(item, x);
          }),
          ...myproducts.entries.map((product) {
            return Container(
              margin: EdgeInsets.all(5),
              width: 100,
              color: Colors.blue.shade200,
              child: Column(
                children: [
                  Text(product.key),
                  Text(product.value.toString()),
                ],
              ),
            );
          }),
        ],
      ),
    );
  }
}
